
require('../../setting/color'); // include for color
const net = require('net');
const fs = require('fs');
const StreamBuffer = require('./custom-stream');
const HIGHWATERMARK = 16384;

// create init server
const server = net.createServer();

Number.isInteger = Number.isInteger || function(value) {
    return typeof value === 'number'
        && Number.isFinite(value)
        && !(value % 1);
};


server.listen(process.env.PORT || 9999, function () {
    console.log('server listening to %j'.info, server.address());
});

// add handler to event 'connection'
server.on('connection', handleConnection);

// is handler
function handleConnection(conn) {
    let remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
    console.log('new client connection from %s'.info, remoteAddress);

    conn.on('data', function (d) {
        console.log('connection data from %s: %j'.debug, remoteAddress, d);
        if (Number.isInteger(parseInt(d.toString())) && parseInt(d.toString()) <= HIGHWATERMARK) {
            let stream = fs.readFileSync(`${__dirname}/file.txt`);
            new StreamBuffer(stream, {
                highWaterMark : parseInt(d.toString()),
            }).pipe(conn)

        } else{
            conn.write(d);
        }
    });

    conn.once('close', function () {
        console.log('connection from %s closed'.yellow, remoteAddress);
    });

    conn.on('error', function (err) {
        console.log('Connection %s error: %s'.error, remoteAddress, err.message);
    });
}