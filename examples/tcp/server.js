
require('../../setting/color'); // include for color
const net = require('net');


// create init server
const server = net.createServer();

server.listen(process.env.PORT || 9999, function () {
    console.log('server listening to %j'.info, server.address());
});

// add handler to event 'connection'
server.on('connection', handleConnection);

// is handler
function handleConnection(conn) {
    var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
    console.log('new client connection from %s'.info, remoteAddress);

    conn.on('data', function (d) {
        console.log('connection data from %s: %j'.debug, remoteAddress, d);
        conn.write(d);
    });

    conn.once('close', function () {
        console.log('connection from %s closed'.yellow, remoteAddress);
    });

    conn.on('error', function (err) {
        console.log('Connection %s error: %s'.error, remoteAddress, err.message);
    });
}